# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
點擊筆的按鈕即會將功能切換成筆的功能，下面的drawstyle可以控制筆刷的形狀，有Line、Circle、Triangle、rectangle。
點擊相皮擦可以將模式切換成橡皮擦。
點擊圓形圖案的按鈕可以切換成換圓形的模式，點擊Canvas的任意點並繼續拖曳可以控制圓的長度寬度大小，若是不一樣大則會變成橢圓形。
點擊三角形圖示的按鈕可以將模式換成畫等腰三角形，點擊Canvas的任意點並繼續拖曳可以控制三角形的形狀。
點擊長方形圖示的按鈕可以畫長方形，點擊Canvas的任意點並繼續拖曳可以控制長方形的長寬。
點擊正方形圖示的按鈕可以畫正方形，點擊Canvas的任意點並繼續拖曳可以控制正方形的大小。
點擊弧線圖示的按鈕可以畫出弧線，點擊Canvas的任意點拖曳後在其他地方放開可以控制起點與終點，接著放開的同時可以繼續控制弧線彎曲程度，滑鼠再點擊一下則完成一條弧線。
點擊"T"圖示可以切換成輸入文字模式，點擊Canvas的任意點並拖曳可以控制文字方塊的大小。下面size可以控制字型大小，typeface可以選擇字型，並設定cursor為text。
再來兩個按鈕左邊是上傳，可以將圖片檔，上傳至Canvas，右邊是下載，可以將目前Canvas上的畫的成品以png檔的方式下載。
接著逆時針是undo，順時針是redo。
對於筆刷功能以及畫形狀的功能，可以在choose color選擇顏色，可以在Linewidth控制寬度，並設定cursor為crosshair。
對於橡皮擦功能，可以在Eraserwidth控制橡皮擦大小，設定cursor為crosshair。
下面垃圾桶圖示可以清掉目前Canvas上的東西(可以再透過undo恢復原狀)。

在js檔裡面以
clicked_pen
clicked_eraser
clicked_circle
clicked_triangle
clicked_rectangle
clicked_text
clicked_square
clicked_arc
去控制state
為了輸入文字進去，有一個引用函式庫"https://goldfirestudios.com/proj/canvasinput/CanvasInput.min.js?v=1.2.0"

extra: 畫正方形及弧線
