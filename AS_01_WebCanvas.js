var clicked_pen=0;
var clicked_eraser=0;
var clicked_circle=0;
var clicked_triangle=0;
var clicked_rectangle=0;
var clicked_text=0;
var clicked_square=0;
var clicked_arc=0;
var m_down=0;
var m_move=0;
var m_over=0;
var cPushArray=new Array();
var astep=-1;
var bstep=-1;
var pointX=0,pointY=0;
var pointX_=0,pointY_=0;
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext("2d");
var img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);
var drawing=false;
var drawn=false;
var flag=0;
var flag1=0;
var flag2=0;
window.addEventListener("popstate",cdo,false);
window.addEventListener("pushstate",cdo,false);
 /*new CanvasText( myCanvas, {
    x: 300,
    y: 300,
    width: 300,
    placeholder: 'Enter your username...'
} );*/
/*var input = new CanvasInput({
    canvas: document.getElementById('myCanvas'),
    fontSize: 18,
    fontFamily: 'Arial',
    fontColor: '#212121',
    fontWeight: 'bold',
    width: 300,
    padding: 8,
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 3,
    boxShadow: '1px 1px 0px #fff',
    innerShadow: '0px 0px 5px rgba(0, 0, 0, 0.5)',
    placeHolder: 'Enter message here...'
  });*/
function cUndo(){
    if(astep>0){
        window.history.back();
        astep--;
    }
}
function cRedo(){
    if(astep<bstep){
        window.history.forward();
        astep++;
    }
}
function cdo(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if( event.state ){
    ctx.putImageData(event.state, 0, 0);
    }
    //console.log("a="+astep+" b="+bstep);
}
function mousedown(){
    m_down=1;
}
function mouseup(){
    m_down=0;
}
function mousemove(){
    m_move=1;
    document.getElementById("myCanvas").addEventListener("mousemove", function(event) {
        if(flag==0){
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext("2d");
            ctx.fillStyle = '#f0ccab';
            ctx.fillRect(0,0,800,650);
            let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
            window.history.pushState(state, null);
            astep++;
            bstep++;
            flag=1;
        }
        if(clicked_pen==1){
                  
            if(document.getElementById("drawstyle").value=="Line"){
                var x=event.clientX,y=event.clientY;
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';
                ctx.strokeStyle= document.getElementById("mycolor").value;
                if(event.buttons==1){
                    ctx.lineTo(x-160,y+1);
                    ctx.stroke();
                    drawing=true;
                }
                else{
                    ctx.beginPath();
                    ctx.moveTo(x-160, y); 
                    if(drawing==true){
                        drawing=false;
                        let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                        window.history.pushState(state, null);
                        astep++;bstep++;
                    }              
                }     
            } 
            else if(document.getElementById("drawstyle").value=="Circle"){
                var x=event.clientX,y=event.clientY;
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                ctx.fillStyle= document.getElementById("mycolor").value;
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';
                ctx.strokeStyle= document.getElementById("mycolor").value;
                if(event.buttons==1){
                    ctx.beginPath();
                    ctx.arc(event.clientX-160,event.clientY, ctx.lineWidth,0, 2 * Math.PI);
                    ctx.fill();
                    drawing=true;
                }
                else{                   
                    
                    if(drawing==true){
                        drawing=false;
                        let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                        window.history.pushState(state, null);
                        astep++;bstep++;
                    }              
                }     
            } 
            else if(document.getElementById("drawstyle").value=="Triangle"){
                var x=event.clientX,y=event.clientY;
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                ctx.fillStyle= document.getElementById("mycolor").value;
                ctx.strokeStyle= document.getElementById("mycolor").value;
                if(event.buttons==1){
                    ctx.beginPath();
                    ctx.moveTo(event.clientX-160,event.clientY-ctx.lineWidth);
                    ctx.lineTo(event.clientX-160+Math.sqrt(3)/2*ctx.lineWidth,event.clientY+ctx.lineWidth/2);
                    ctx.lineTo(event.clientX-160-Math.sqrt(3)/2*ctx.lineWidth,event.clientY+ctx.lineWidth/2);
                    ctx.fill();
                    drawing=true;
                }
                else{ 
                    if(drawing==true){
                        drawing=false;
                        let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                        window.history.pushState(state, null);
                        astep++;bstep++;
                    }              
                }     
            } 
            else if(document.getElementById("drawstyle").value=="Rectangle"){
                var x=event.clientX,y=event.clientY;
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                ctx.fillStyle= document.getElementById("mycolor").value;
                ctx.strokeStyle= document.getElementById("mycolor").value;
                if(event.buttons==1){
                    ctx.beginPath();
                    ctx.fillRect(event.clientX-160-ctx.lineWidth,event.clientY-ctx.lineWidth,2*ctx.lineWidth,2*ctx.lineWidth);
                    ctx.fill();
                    drawing=true;
                }
                else{
                    if(drawing==true){
                        drawing=false;
                        let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                        window.history.pushState(state, null);
                        astep++;bstep++;
                    }              
                }     
            } 
        }
        else if(clicked_eraser==1){
            var x=event.clientX,y=event.clientY;
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext("2d");
            ctx.lineWidth = document.getElementById("myeraserwidthRange").value;
            ctx.strokeStyle = '#f0ccab';
            //ctx.lineCap = "round";
            if(event.buttons==1){
                ctx.lineTo(x-160,y+1);
                ctx.stroke();
                drawing=true;
            }
            else{
                ctx.beginPath();
                ctx.moveTo(x-160, y);   
                if(drawing==true){
                    drawing=false;
                    let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                    window.history.pushState(state, null);
                    astep++;bstep++;
                }   
                
            }             
        }
        else if(clicked_circle==1){
            if(m_down&&drawing==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                pointX=event.clientX-160;
                pointY=event.clientY;
                drawing=true;
                //console.log("123");
                img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
            }
            if(event.buttons==1){
                
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';  
                ctx.strokeStyle= document.getElementById("mycolor").value;
                ctx.ellipse((pointX+event.clientX-160)/2, (pointY+event.clientY)/2,Math.abs((pointX-event.clientX+160)/2), Math.abs((pointY-event.clientY)/2),0,0,2*Math.PI,false);
                ctx.stroke();      
                //console.log("down="+m_down+" "+pointX+" "+pointY+" x="+event.clientX+",y="+event.clientY);
            }
            if(m_down==0&&drawing==true){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                drawing=false;
                let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
                astep++;bstep++;
            }
        }
        else if(clicked_triangle==1){
            if(m_down&&drawing==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                pointX=event.clientX-160;
                pointY=event.clientY;
                drawing=true;
                //console.log("123");
                img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
            }
            if(event.buttons==1){
                
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';  
                ctx.strokeStyle= document.getElementById("mycolor").value;
                ctx.moveTo(pointX,pointY);
                ctx.lineTo(event.clientX-160,event.clientY);
                ctx.lineTo(2*pointX-event.clientX+160,event.clientY);
                ctx.lineTo(pointX,pointY);
                ctx.lineTo(event.clientX-160,event.clientY);
                ctx.stroke();      
                //console.log("down="+m_down+" "+pointX+" "+pointY+" x="+event.clientX+",y="+event.clientY);
            }
            if(m_down==0&&drawing==true){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                drawing=false;
                let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
                astep++;bstep++;
            }
        }
        else if(clicked_rectangle==1){
            
            if(m_down&&drawing==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                pointX=event.clientX-160;
                pointY=event.clientY;
                drawing=true;
                //console.log("123");
                img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
            }
            if(event.buttons==1){
                
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';
                ctx.strokeStyle= document.getElementById("mycolor").value;
                ctx.rect(pointX,pointY,event.clientX-160-pointX,event.clientY-pointY);  
                ctx.stroke();      
                //console.log("down="+m_down+" "+pointX+" "+pointY+" x="+event.clientX+",y="+event.clientY);
            }
            if(m_down==0&&drawing==true){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                drawing=false;
                let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
                astep++;bstep++;
            }
        
        }
        else if(clicked_text){
            
            if(m_down&&drawing==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                pointX=event.clientX-160;
                pointY=event.clientY;
                drawing=true;
                //console.log("123");
                img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
            }
            if(event.buttons==1){
                
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = "1";
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';
                ctx.strokeStyle= "rgb(0,0,0)";
                ctx.rect(pointX,pointY,event.clientX-160-pointX,event.clientY-pointY);  
                ctx.stroke();      
                //console.log("down="+m_down+" "+pointX+" "+pointY+" x="+event.clientX+",y="+event.clientY);
            }
            if(m_down==0&&drawing==true){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                drawing=false;
                let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
                astep++;bstep++;
                ctx.putImageData(img_data,0,0);
                var input_name = new CanvasInput({
                    canvas: document.getElementById('myCanvas'),
                    x: pointX-2,
                    y: pointY-2,
                    //fontSize: event.clientY-pointY+5,
                    fontSize: document.getElementById("fontsize").value*2,
                    fontFamily: document.getElementById("fontfamily").value,
                    fontColor: '#212121',
                    fontWeight: 'bold',
                    width: event.clientX-pointX-160,
                    height: Math.max(event.clientY-pointY,document.getElementById("fontsize").value*2) ,
                    backgroundColor: '#f0ccab',
                    //backgroundColor: 'rgba(255,100,100,0.2)',
                    padding: 5,
                    borderWidth: 0,
                    //borderColor: '#000',
                    //borderRadius: 3,
                    borderColor: '#f0ccab',
                    boxShadow: '0px 0px 0px #f0ccab',
                    innerShadow: '0px 0px 5px rgba(0, 0, 0, 0)',
                    placeHolder: ''
                    
                  });
                
            }
        }
        else if(clicked_square==1){
            
            if(m_down&&drawing==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                pointX=event.clientX-160;
                pointY=event.clientY;
                drawing=true;
                //console.log("123");
                img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
            }
            if(event.buttons==1){
                
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                //ctx.strokeStyle = 'rgb('+document.getElementById("myredRange").value+','+document.getElementById("mygreenRange").value+','+document.getElementById("myblueRange").value+')';
                ctx.strokeStyle= document.getElementById("mycolor").value;
                if(Math.abs(event.clientX-160-pointX)>Math.abs(event.clientY-pointY)){
                    ctx.rect(pointX-Math.abs(event.clientX-160-pointX),pointY-Math.abs(event.clientX-160-pointX),2*Math.abs(event.clientX-160-pointX),2*Math.abs(event.clientX-160-pointX));
                }
                else{
                    ctx.rect(pointX-Math.abs(event.clientY-pointY),pointY-Math.abs(event.clientY-pointY),2*Math.abs(event.clientY-pointY),2*Math.abs(event.clientY-pointY));
                }
                ctx.stroke();      
                //console.log("down="+m_down+" "+pointX+" "+pointY+" x="+event.clientX+",y="+event.clientY);
            }
            if(m_down==0&&drawing==true){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                drawing=false;
                let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
                astep++;bstep++;
            }
        
        }
        else if(clicked_arc==1){
            
            if(m_down&&drawing==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                pointX=event.clientX-160;
                pointY=event.clientY;
                drawing=true;
                //console.log("123");
                img_data=ctx.getImageData(0,0,canvas.width,canvas.height);
            }
            else if(event.buttons==1){
                pointX_=event.clientX-160;
                pointY_=event.clientY;
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                ctx.strokeStyle= document.getElementById("mycolor").value;
                ctx.moveTo(pointX,pointY);
                ctx.quadraticCurveTo(pointX,pointY,event.clientX-160,event.clientY,pointX-160,pointY);
                ctx.stroke();      
                //console.log("down="+m_down+" "+pointX+" "+pointY+" x="+event.clientX+",y="+event.clientY);
            }
            else if(event.buttons==0&&drawing==true&&drawn==false){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.putImageData(img_data,0,0);
                ctx.beginPath();
                ctx.lineWidth = document.getElementById("mylinewidthRange").value;
                ctx.strokeStyle= document.getElementById("mycolor").value;
                ctx.moveTo(pointX,pointY);
                ctx.quadraticCurveTo(event.clientX-160,event.clientY,pointX_,pointY_);
                ctx.stroke(); 
                flag2=1;
                //drawn=true; 
            }
            
            else if(m_down==0&&drawing==true&&drawn==true){
                var c = document.getElementById("myCanvas");
                var ctx = c.getContext("2d");
                drawing=false;
                drawn=false;
                flag2=0;
                //console.log("@@");
                let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
                astep++;bstep++;
            }
        
        }
    });  
}
function mouseclick(){
if(flag2==1)drawn=true;
//console.log("!!!!!!");
}
function mouseover(){
    m_over=1;
}
function mouseout(){
    m_over=0;
}
function clickpen(){
    clicked_pen=1;
    clicked_eraser=0;
    clicked_circle=0;
    clicked_triangle=0;
    clicked_rectangle=0;
    clicked_text=0;
    clicked_square=0;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clickeraser(){
    clicked_eraser=1;
    clicked_pen=0;
    clicked_circle=0;
    clicked_triangle=0;
    clicked_rectangle=0;
    clicked_text=0;
    clicked_square=0;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clickcircle(){
    clicked_eraser=0;
    clicked_pen=0;
    clicked_circle=1;
    clicked_triangle=0;
    clicked_rectangle=0;
    clicked_text=0;
    clicked_square=0;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clicktriangle(){
    clicked_eraser=0;
    clicked_pen=0;
    clicked_circle=0;
    clicked_triangle=1;
    clicked_rectangle=0;
    clicked_text=0;
    clicked_square=0;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clickrectangle(){
    clicked_eraser=0;
    clicked_pen=0;
    clicked_circle=0;
    clicked_triangle=0;
    clicked_rectangle=1;
    clicked_text=0;
    clicked_square=0;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clicktext(){
    clicked_eraser=0;
    clicked_pen=0;
    clicked_circle=0;
    clicked_triangle=0;
    clicked_rectangle=0;
    clicked_text=1;
    clicked_square=0;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),text";
}
function clicksquare(){
    clicked_eraser=0;
    clicked_pen=0;
    clicked_circle=0;
    clicked_triangle=0;
    clicked_rectangle=0;
    clicked_text=0;
    clicked_square=1;
    clicked_arc=0;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clickarc(){
    clicked_eraser=0;
    clicked_pen=0;
    clicked_circle=0;
    clicked_triangle=0;
    clicked_rectangle=0;
    clicked_text=0;
    clicked_square=0;
    clicked_arc=1;
    document.getElementById("myCanvas").style.cursor="url('pen.png'),crosshair";
}
function clean(){
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = '#f0ccab';
    ctx.fillRect(0,0,800,650);
    let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
    astep++;bstep++;
}
function trashover(){   
    if(event.buttons==0){
        document.getElementById("trashcan").style.width="72px";
        document.getElementById("trashcan").style.height="60px";
    }
    document.getElementById("trashcan").style.cursor="pointer";
}
function trashout(){
    document.getElementById("trashcan").style.width="60px";
    document.getElementById("trashcan").style.height="50px";
    document.getElementById("trashcan").style.cursor="default";
}
function trashdown(){
    document.getElementById("trashcan").style.width="60px";
    document.getElementById("trashcan").style.height="50px";
}
function trashup(){
    document.getElementById("trashcan").style.width="72px";
    document.getElementById("trashcan").style.height="60px";
}

function startdownload(){
    download(document.getElementById("myCanvas"), 'image.png');
}
function download(canvas, filename) {
    /// create an "off-screen" anchor tag
    var lnk = document.createElement('a'), e;
  
    /// the key here is to set the download attribute of the a tag
    lnk.download = filename;
  
    /// convert canvas content to data-uri for link. When download
    /// attribute is set the content pointed to by link will be
    /// pushed as "download" in HTML5 capable browsers
    lnk.href = canvas.toDataURL("image/png;base64");
  
    /// create a "fake" click-event to trigger the download
    if (document.createEvent) {
      e = document.createEvent("MouseEvents");
      e.initMouseEvent("click", true, true, window,
                       0, 0, 0, 0, 0, false, false, false,
                       false, 0, null);
  
      lnk.dispatchEvent(e);
    } else if (lnk.fireEvent) {
      lnk.fireEvent("onclick");
    }
}
document.getElementById('inp').onchange = function(e) {
    var img = new Image();
    img.onload = draw;
    img.onerror = failed;
    img.src = URL.createObjectURL(this.files[0]);
  };
function draw() {
    var canvas = document.getElementById('myCanvas');
    canvas.width = this.width;
    canvas.height = this.height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(this, 0,0);
    let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
    astep++;bstep++;
}
function failed() {
    console.error("The provided file couldn't be loaded as an Image media");
}
function mycursor(){
    //alert("123");
    //document.getElementById(container).style.cursor="url('pen.png'),auto";
}
